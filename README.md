# Rili

Rili is future oriented set of tools and modular C++ libraries for rapid and prototype programming, which provide constructions based on design patterns and most useful paradigms common in other languages, but not trivially implementable in C++.

This project accumulate all rili modules as single dependency. Each stable version is created based on latest stable rili modules versions.

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/rili/badges/master/build.svg)](https://gitlab.com/rilis/rili/rili/commits/master)
