[Rili](https://gitlab.com/rilis/rili) is set of modular, future oriented C++ libraries for rapid and prototype programming,
which provide tools and constructions based on design patterns and most
useful concepts common in other languages, but not trivially implementable in C++.
It's based on modern C++ features, so C++11 is required, but we strongly recommend at least
C++14 as it have much better lambda functions syntax - with smarter auto type specifier.

### Rili main goals

Rili is mostly focused on:

  * provading ready to use tools and concepts,
  * help developers create clean code and application architecture,
  * introducing not error prone, easy to understand and use API,
  * being easy to extend and integrate with projects,
  * making software testable on all stages,
  * making software easier to understand and maintain

Rili constantly tries to improve own quality and development practices.
In addition Rili promote automatization of software production and maintenance process.

### Why Rili was created?

C++ is beautifull, expressive, powerfull and very flexible language,however in
standard library, we don't have many things which we have out of the box in other
modern languages or standard don't try to cover some areas covered typically in
other languages - for example filesystem, networking async programmming.
This introduce huge mess. Additionally most of C++ libraries provide only primitives
on top of which we can buid something usefull. But this is in typical applications
reinventing wheel again and again. Solving the same bugs, without moving forward.

For instance we have a lot of really small and handy modules in nodejs (even if their quality sometimes is poor), the same for python.

Rili try to be answer on lack of integrated but still modular, ready to use "tools" for daily C++
applications development - idea of Rili is to deliver "good enaught" solutions addressed to real needs.
